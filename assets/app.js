/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './css/app.scss';
// import './styles/app.css';

// start the Stimulus application
// import './bootstrap';

// Need jQuery? Install it with "yarn add jquery", then uncomment to import it.
import $ from 'jquery';

import './js/ajaxForm.js';

import 'featherlight';
import "lazy-load-images";

$(function() {
  // AOS initialization
  AOS.init({
  offset:     120,
  delay:      0,
  easing:     'ease',
  duration:   200,
  disable:    false, // Condition when AOS should be disabled. e.g. 'mobile'
  once:       true,
  mirror:     false, // whether elements should animate out while scrolling past them
  startEvent: 'DOMContentLoaded',
  useClassNames: true,
  });
});

$(function() {
  let images = document.querySelectorAll("img");
  images.forEach(image => {
    image.setAttribute('data-lazy-load-src', image.getAttribute('src'));
  });

  let lazyLoadImages = require('lazy-load-images');

  lazyLoadImages.init();
});





$(function() {
// Intersection Observer
  const sectionOne = document.querySelector(".about");
  const sections = document.querySelectorAll("section");

  const options = {
    root: null,
    threshold: 0,
    rootMargin: "-150px"
  };

  const observer = new IntersectionObserver(function
    (entries, observer) {
      entries.forEach(entry => {
        console.log("New Item Found: ");
        console.log(entry.target);
      });
    }, options);

  sections.forEach(section => {
    observer.observe(section);
  });
});
