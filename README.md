# A Symfony website example

Two backend interface, EasyAdminBundle and SonataAdminBundle    
Assets managed with npm-yarn, webpack, sass    
Documentation built with phpDocumentor

### Install
Check and download dependencies    
        `composer install`    
        `symfony check:requirements`    
#### DB Setup        
        mysql -u root -p    
        CREATE USER 'test001'@'localhost' IDENTIFIED BY 'test001';    
        GRANT ALL PRIVILEGES ON test001.* TO 'test001'@'localhost';    
        FLUSH PRIVILEGES;    
        exit;    

        cp .env .env.local
Set DATABASE_URL according with your system

#### Create database schema
        php bin/console doctrine:database:create
        php bin/console doctrine:schema:create
        php bin/console doctrine:migrations:migrate

#### Load all fixtures
        php bin/console doctrine:fixtures:load --append

#### Add node dependencies and run webpack
        yarn install
        yarn encore production

#### Run the project
        symfony server:start

## example Apache configuration
    <VirtualHost *:80>
         ServerAdmin admin@example.com
         DocumentRoot /var/www/www.example.com/public
         ServerName example.com
         ServerAlias www.example.com

         <Directory /var/www/www.example.com/public/>
              Options FollowSymlinks
              AllowOverride All
              Require all granted
         </Directory>

         ErrorLog ${APACHE_LOG_DIR}/error.log
         CustomLog ${APACHE_LOG_DIR}/access.log combined
    </VirtualHost>
