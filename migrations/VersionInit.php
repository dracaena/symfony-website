<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class VersionInit extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->workers($schema);
        $this->pages($schema);
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE user');
    }

    public function workers(Schema $schema) : void
    {
        $this->addSql("
        INSERT INTO `workers` (`id`, `name`, `description`, `lastname`) VALUES
        (null,	'Daniele',	'Fondatore, esperto di tostatura e assggiatore professionista di caffè.',	'Rossi'),
        (null,	'Giacomo',	'Team manager, giornalista e appassionato di cucina.',	'Mancini'),
        (null,	'Marina',	'Resposabile di produzione, appassionata di viaggi enogastronomici e aggiornatissima sulle nuove tendenze.',	'Sesti');
        ");
    }

    public function pages(Schema $schema) : void
    {
        $this->addSql("
        INSERT INTO `pages` (`id`, `title`, `view_order`, `enabled`, `url`, `enabled_user`, `view_order_user`) VALUES
        (null,	'home',	1,	1,	'homepage',	1,	1),
        (null,	'contatti',	2,	1,	'contacts',	0,	NULL),
        (null,	'eventi',	NULL,	0,	'events',	1,	2),
        (null,	'prodotti',	3,	1,	'products',	1,	3),
        (null,	'registrati',	4,	1,	'app_register',	0,	NULL),
        (null,	'profilo',	NULL,	0,	'profile',	1,	4),
        (null,	'carrello',	NULL,	0,	'cart',	1,	5),
        (null,	'ordini',	NULL,	0,	'orders',	1,	6);
        ");
    }

}
