<?php

namespace App\Twig;

use App\Entity\User;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Security;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{
    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var Security
     */
    private $security;

    /**
     * @var UrlGeneratorInterface
     */
    private $router;

    public function __construct(RequestStack $requestStack, Security $security, UrlGeneratorInterface $router)
    {
        $this->requestStack = $requestStack;
        $this->security = $security;
        $this->router = $router;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            new TwigFunction('menu_class', [$this, 'getMenuClass']),
        ];
    }

    /**
     * @param string|string[] $routes
     * @param string          $class
     *
     * @return string
     */
    public function getMenuClass($routes, $class = 'active')
    {
        $request = $this->requestStack->getCurrentRequest();

        // null request
        if (!$request) {
            return '';
        }

        $route = $request->get($routes);

        echo $route;
        $isCurrent = false;
        if (is_array($routes)) {
            $isCurrent = in_array($route, $routes);
        } else {
            $isCurrent = $route === $routes;
        }

        // return $isCurrent ? $class : '';
    }

}
