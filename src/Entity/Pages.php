<?php

namespace App\Entity;

use App\Repository\PagesRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=PagesRepository::class)
 */
class Pages
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $view_order;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $view_order_user;

    /**
     * @ORM\Column(type="boolean")
     */
    private $enabled;

    /**
     * @ORM\Column(type="boolean")
     */
    private $enabled_user;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $url;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getViewOrder(): ?int
    {
        return $this->view_order;
    }

    public function setViewOrder(?int $view_order): self
    {
        $this->view_order = $view_order;

        return $this;
    }

    public function getEnabled(): ?bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }


    public function getEnabledUser(): ?bool
    {
        return $this->enabled_user;
    }

    public function setEnabledUser(bool $enabled_user): self
    {
        $this->enabled_user = $enabled_user;

        return $this;
    }

    public function getViewOrderUser(): ?int
    {
        return $this->view_order_user;
    }

    public function setViewOrderUser(?int $view_order_user): self
    {
        $this->view_order_user = $view_order_user;

        return $this;
    }

}
