<?php

namespace App\Form;

use App\Entity\OrderItem;
use App\Form\EventListener\RemoveCartItemListener;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class CartItemType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
      $choices=[];

      for ($i=1; $i < 30; $i++) {
        $choices[$i] = $i;
      }

        $builder
            ->add('quantity', ChoiceType::class, [
              'required' => true,
              'multiple' => false,
              'expanded' => false,
              'choices' => $choices,
            ])
            ->add('remove', SubmitType::class)
        ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => OrderItem::class,
        ]);
    }
}
