<?php

namespace App\Form;

use App\Entity\Messages;
use App\Entity\Workers;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;


class MessagesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('subject')
            ->add('object')
            ->add('email')
            ->add('workers', EntityType::class, [
              'class' => Workers::class,
              'choice_label' => 'name',
              'query_builder' => function (EntityRepository $repo) {
                  return $repo->createQueryBuilder('f')
                      ->where('f.id > :id')
                      ->setParameter('id', 1);
              },
              'expanded' => true,
              'multiple'  => true,

              // 'entry_type' => TextType::class,
              // 'required' => true,
            ])
            ->add('send', SubmitType::class)

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Messages::class,
        ]);
    }
}
