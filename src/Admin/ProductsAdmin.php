<?php

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;


use Sonata\MediaBundle\Form\Type\MediaType;


final class ProductsAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
          ->add('name', TextType::class)
          ->add('description')
          ->add('price')
          ->add('quantity')
          ->add('imageFile', FileType::class, array(
                'data_class' => null,
                'allow_file_upload' => true,
                'required' => false,
            ));

          // ->add('roles', CollectionType::class)
          // ->add('password')
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
          ->add('image', NULL, array('template' => 'admin/image.html.twig'))
          ->addIdentifier('name')
          ->add('createdAt')
          ->add('updatedAt')
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper): void
    {
        parent::configureShowFields($showMapper);

        $showMapper
            ->add('name')
            ->add('description')
            ->reorder([
                'id',
                'createdAt',
                'image',
            ])
        ;
    }

}
