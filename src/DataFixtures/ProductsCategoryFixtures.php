<?php

namespace App\DataFixtures;

use App\Entity\ProductsCategory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ProductsCategoryFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $cat1 = new ProductsCategory();
        $cat2 = new ProductsCategory();
        $cat3 = new ProductsCategory();

        $cat1
            ->setName('Arabica')
            ->setImage('V1.jpg');
        $cat2
            ->setName('Robusta')
            ->setImage('V2.jpg');
        $cat3
            ->setName('Excelsa')
            ->setImage('V3.jpg');

        $manager->persist($cat1);
        $manager->persist($cat2);
        $manager->persist($cat3);
        $manager->flush();
    }
}
