<?php

namespace App\DataFixtures;

use App\Entity\Events;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Validator\Constraints\DateTime;

class EventsFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {

      for ($i = 1; $i <= 10; $i++) {
          $product = new Events();

          $product
              ->setName('Event ' . $i)
              ->setDescription('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua')
              ->setPrice(mt_rand(10, 50))
              ->setTotalTickets(mt_rand(20, 50))
              ->setImage('V2.jpg')
              ->setEventDatetime(new \DateTime(sprintf('-%d days', rand(1, 100))))
              ->setCity('Firenze')
              ->setAddress('via Giacomo Matteotti')
              ->setAddressNumber('24')
          ;
          $manager->persist($product);
      }
        $manager->flush();
    }
}
