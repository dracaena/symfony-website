<?php

namespace App\Controller;

use App\Entity\Orders;
use App\Form\OrdersType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class OrdersController extends AbstractController
{
    /**
     * @Route("/orders", name="orders")
     */
    public function index(Request $request): Response
    {
      $id = $this->getUser('id');
      $em = $this->getDoctrine()->getManager();

      $orders = $em->getRepository(Orders::class)->findBy([
        'user' => $id
      ]);

      $form = $this->createForm(OrdersType::class);
      $form->handleRequest($request);

      if ($form->isSubmitted() && $form->isValid()) {
          $item = $form->getData();
          $item
              ->setUpdatedAt(new \DateTime());

          $em->persist($item);
          $em->flush();

          return $this->redirectToRoute('orders');
      }

      return $this->render('orders/index.html.twig', [
          'orders' => $orders,
          'form' => $form->createView()
      ]);

    }
}
