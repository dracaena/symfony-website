<?php

namespace App\Controller;

use App\Entity\Pages;
use App\Entity\ProductsCategory;
use App\Entity\Workers;
use App\Entity\User;
use App\Form\ProfileType;
use App\Form\MessagesType;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;


class DefaultController extends AbstractController
{

    /**
     * @Route("/", name="homepage")
     */
    public function index(AuthenticationUtils $authenticationUtils): Response
    {
      // get the login error if there is one
      $error = $authenticationUtils->getLastAuthenticationError();
      // last username entered by the user
      $lastUsername = $authenticationUtils->getLastUsername();

      $em = $this->getDoctrine()->getManager();
      $workers = $em->getRepository(Workers::class)->findAll();
      $productsCategory = $em->getRepository(ProductsCategory::class)->findAll();

        return $this->render('default/index.html.twig', [
            'last_username' => $lastUsername,
            'error' => $error,
            'workers' => $workers,
            'products_category' => $productsCategory
        ]);
    }

    /**
     * @Route("/contacts", name="contacts")
     */
    public function contacts(Request $request): Response
    {
      $form = $this->createForm(MessagesType::class);
      $form->handleRequest($request);

      if ($form->isSubmitted() && $form->isValid()) {

          $entityManager = $this->getDoctrine()->getManager();

          $message = $form->getData();
          $entityManager->persist($message);
          $entityManager->flush();

          return $this->redirectToRoute('contacts');
      }

      return $this->render('default/contacts.html.twig', [
          'form' => $form->createView(),
      ]);
    }

    /**
     * @Route("/profile", name="profile")
     */
    public function profile(Request $request): Response
    {
        $user = $this->getUser();

        $form = $this->createForm(ProfileType::class);
        $form->handleRequest($request);


        return $this->render('default/profile.html.twig', [
            'controller_name' => 'ProfileController',
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/privacy-policy", name="privacy_policy")
     */
    public function privacyPolicy(): Response
    {
        return $this->render('default/privacy_policy.html.twig');
    }

    /**
     * @Route("/cookie-policy", name="cookie_policy")
     */
    public function cookiePolicy(): Response
    {
        return $this->render('default/cookie_policy.html.twig');
    }


}
