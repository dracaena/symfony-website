<?php

namespace App\Controller;

use App\Entity\Products;
use App\Entity\ProductsCategory;
use App\Form\AddToCartType;
use App\Manager\CartManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ProductsController extends AbstractController
{
    /**
     * @Route("/products", name="products")
     */
    public function index(): Response
    {
        $em = $this->getDoctrine()->getManager();

        $products = $em->getRepository(Products::class)->findAll();

        return $this->render('products/index.html.twig', [
            'products_list'   => $products,
        ]);
    }

    /**
     * @Route("/products/{id}", name="products_show")
     */
    public function show($id, Products $product, Request $request, CartManager $cartManager): Response
    {
        $em = $this->getDoctrine()->getManager();

        $products = $em->getRepository(Products::class)->findOneBy([
          'id' => $id
        ]);

        $user = $this->getUser();

        $form = $this->createForm(AddToCartType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $item = $form->getData();
            $item->setProduct($product);

            $cart = $cartManager->getCurrentCart();
            $cart
                ->addItem($item)
                ->setUpdatedAt(new \DateTime())
                ->setUser($user);

            $cartManager->save($cart);

            return $this->redirectToRoute('products_show', ['id' => $product->getId()]);
        }

        return $this->render('products/show.html.twig', [
            'product'         => $products,
            'form'            => $form->createView()
        ]);
    }


}
