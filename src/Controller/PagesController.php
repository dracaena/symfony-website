<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use App\Entity\Pages;

class PagesController extends AbstractController
{
    /**
     * @Route("/pages", name="pages")
     */
    public function index(): Response
    {
      $em = $this->getDoctrine()->getManager();
      $user = $this->getUser();

      if ($user == null )
      {
        $pages = $em->getRepository(Pages::class)->findBy(
          [ 'enabled' => 'true', ],
          [ 'view_order' => 'ASC' ]
        );
      } elseif (in_array("ROLE_USER", $user->getRoles())) {
        $pages = $em->getRepository(Pages::class)->findBy(
          [ 'enabled_user' => 'true', ],
          [ 'view_order_user' => 'ASC' ]
        );
      }

      return $this->render('shared/_header.html.twig', [
          'pages_list' => $pages,
          'user' => $user
      ]);
    }
}
