<?php

namespace App\Controller\Admin;

use App\Entity\ProductsCategory;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class ProductsCategoryCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return ProductsCategory::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('name', 'Name'),
            TextField::new('parent', 'Parent'),
            AssociationField::new('products', 'Products'),
            ImageField::new('imageFile')
            ->hideOnIndex()
            ->hideOnDetail()
            ->setBasePath('/upload/images/products'),
            ImageField::new('image')
            ->hideOnForm()
            ->setTemplatePath('products/image.html.twig')
            ->setBasePath('/upload/images/products')
        ];
    }
    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
