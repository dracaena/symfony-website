<?php

namespace App\Controller\Admin;

use App\Entity\Products;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;

use App\Form\AttachmentType;

use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;


class ProductsCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Products::class;
    }

    public function configureFields(string $pageName): iterable
    {
      $fields = [
            TextField::new('name', 'Name'),
            TextField::new('description', 'Description')
            ->hideOnIndex(),
            IntegerField::new('price', 'Price'),
            IntegerField::new('quantity', 'Quantity'),
            DateTimeField::new('createdAt')
            ->hideOnForm(),
            DateTimeField::new('updatedAt')
            ->onlyOnDetail(),
            TextareaField::new('full_description', 'Full description')
            ->hideOnIndex(),
            AssociationField::new('category', 'Category')
            ->autocomplete(),
            CollectionField::new('attachments', 'Attachments')
            ->hideOnIndex()
            ->setEntryType(AttachmentType::class)
            ->setFormTypeOption('by_reference', false)
            // CollectionField::new('attachments', 'Attachments')
            // ->hideOnIndex()
            ->setTemplatePath('images.html.twig'),
            ImageField::new('imageFile')
            ->hideOnIndex()
            ->hideOnDetail()
            ->setBasePath('/upload/images/products'),
            ImageField::new('image')
            ->hideOnForm()
            ->setTemplatePath('products/image.html.twig')
            ->setBasePath('/upload/images/products')

        ];

      return $fields;
    }

    public function configureActions(Actions $actions): Actions
    {
      return $actions->add(CRUD::PAGE_INDEX, Action::DETAIL);
      // return $actions->add(CRUD::PAGE_INDEX, actionNameOrObject: 'delete');
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
