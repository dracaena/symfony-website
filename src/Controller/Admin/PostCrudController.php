<?php

namespace App\Controller\Admin;

use App\Entity\Post;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;

use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;


class PostCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Post::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('title'),
            BooleanField::new('published'),
            TextEditorField::new('content')
            ->hideOnIndex(),
            AssociationField::new('category'),
            DateTimeField::new('createdAt')
            ->onlyOnIndex(),
            DateTimeField::new('updatedAt')
            ->onlyOnIndex(),
            ImageField::new('thumbnailFile')
            ->hideOnIndex()
            ->hideOnDetail()
            ->setBasePath('/upload/images'),
            ImageField::new('thumbnail')
            ->hideOnForm()
            ->setTemplatePath('image.html.twig')
            ->setBasePath('/upload/images')
        ];
    }

    public function configureActions(Actions $actions): Actions
    {
      return $actions->add(CRUD::PAGE_INDEX, Action::DETAIL);
      // return $actions->add(CRUD::PAGE_INDEX, actionNameOrObject: 'delete');
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
