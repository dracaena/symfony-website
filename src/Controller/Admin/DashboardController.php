<?php

namespace App\Controller\Admin;

use App\Entity\User;
use App\Entity\Products;
use App\Entity\ProductsCategory;
use App\Entity\Post;
use App\Entity\PostCategory;
use App\Entity\Pages;
use App\Entity\Workers;
use App\Entity\Orders;
use App\Entity\Events;
use App\Controller\Admin\UserCrudController;
use App\Controller\Admin\PostCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use EasyCorp\Bundle\EasyAdminBundle\Router\CrudUrlGenerator;

class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admins", name="admin")
     */
    public function index(): Response
    {
        $routeBuilder = $this->get(CrudUrlGenerator::class)->build();

        return $this->redirect($routeBuilder->setController(PostCrudController::class)->generateUrl());
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            // the name visible to end users
            ->setTitle('Symfony Vue')
            // you can include HTML contents too (e.g. to link to an image)
            ->setTitle('Black <span class="text-small">Coffee</span>')

            // the path defined in this method is passed to the Twig asset() function
            // ->setFaviconPath('favicon.svg')

            // the domain used by default is 'messages'
            ->setTranslationDomain('my-custom-domain')

            // there's no need to define the "text direction" explicitly because
            // its default value is inferred dynamically from the user locale
            ->setTextDirection('ltr')

            // set this option if you prefer the page content to span the entire
            // browser width, instead of the default design which sets a max width
            ->renderContentMaximized()

            // set this option if you prefer the sidebar (which contains the main menu)
            // to be displayed as a narrow column instead of the default expanded design
            ->renderSidebarMinimized()

            // by default, all backend URLs include a signature hash. If a user changes any
            // query parameter (to "hack" the backend) the signature won't match and EasyAdmin
            // triggers an error. If this causes any issue in your backend, call this method
            // to disable this feature and remove all URL signature checks
            // ->disableUrlSignatures()
        ;
    }

    public function configureMenuItems(): iterable
    {
        return [
          MenuItem::linktoDashboard('Dashboard', 'fa fa-home'),
          MenuItem::linkToCrud('Pages', 'fa fa-file-text', Pages::class),
          MenuItem::linkToCrud('Blog Posts', 'fa fa-file-text', Post::class),
          MenuItem::linkToCrud('Categories', 'fa fa-file-text', PostCategory::class),
          MenuItem::linkToCrud('Events', 'fa fa-file-text', Events::class),
          // ->setAction('new'),

          MenuItem::section('Store'),
          MenuItem::linkToCrud('Orders', 'fa fa-shopping-cart', Orders::class),
          MenuItem::subMenu('Products', 'fa fa-tag', Products::class)->setSubItems([
            MenuItem::linkToCrud('Products List', 'fa fa-tag', Products::class),
            MenuItem::linkToCrud('Products Categories', 'fa fa-archive', ProductsCategory::class),
          ]),

          MenuItem::section('Users'),
          MenuItem::linkToCrud('Users', 'fa fa-group', User::class),
          MenuItem::linkToCrud('Workers', 'fa fa-group', Workers::class),
        ];
    }
}
