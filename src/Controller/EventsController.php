<?php

namespace App\Controller;

use App\Entity\Events;
use App\Form\EventBookType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class EventsController extends AbstractController
{
    /**
     * @Route("/events", name="events")
     */
    public function index(): Response
    {
        $em = $this->getDoctrine()->getManager();

        $events = $em->getRepository(Events::class)->findAll();

        return $this->render('events/index.html.twig', [
            'events_list'   => $events,
        ]);
    }

    /**
     * @Route("/events/{id}", name="events_show")
     */
    public function show($id, Events $event, Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();

        $events = $em->getRepository(Events::class)->findOneBy([
          'id' => $id
        ]);

        $user = $this->getUser();

        $form = $this->createForm(EventBookType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {


            return $this->redirectToRoute('events_show', ['id' => $event->getId()]);
        }

        return $this->render('events/show.html.twig', [
            'product'         => $events,
            'form'            => $form->createView()
        ]);
    }
}
