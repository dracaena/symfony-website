<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    /**
     * @return void
     * @doesNotPerformAssertions
     */
    public function testHomepage()
    {
        $client = static::createClient();
        //
        $client->request('GET', '/');
        //
        // $this->assertResponseIsSuccessful();
    }
}
